#!/usr/bin/env python

from feedmailer import FeedMailer
import os


class Feed2Mail:
    def __init__(self, database):
        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        self.database = database
        self.files = os.listdir(self.script_dir + "/" + database)

    def process(self):
        for f in self.files:
            target_file = open(self.script_dir + "/" + self.database + "/" + f, "r")
            feeds = []
            for line in target_file.readlines():
                feeds.append(line.strip())
            feed_mailer = FeedMailer(f, feeds)
            feed_mailer.mail_feeds()


if __name__ == "__main__":
    feed2mail = Feed2Mail("database")
    feed2mail.process()
